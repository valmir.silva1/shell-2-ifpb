#!/bin/bash

# Atribui os argumentos às variáveis n1, n2 e n3
n1=$1
n2=$2
n3=$3

# Soma os três números e atribui o resultado à variável r
r=$(echo "$n1 + $n2 + $n3" | bc)

# Imprime o resultado
echo "A soma é: $r"


#!/bin/bash

# Pedir ao usuário para digitar um número inteiro
echo "Digite um número inteiro:"
read x

# Calcular o valor de y
y=$((x*x + 7))

# Imprimir os valores de x e y
echo "O valor de x é $x"
echo "O valor de y é $y"

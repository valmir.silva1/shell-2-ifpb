#!/bin/bash

# Exibe informações sobre o hardware
echo "Hostname: $HOSTNAME"
echo "Endereço MAC: $(ip link | grep -o -E '([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}')"
echo "CPU: $(lscpu | grep 'Model name' | awk -F: '{print $2}' | sed 's/^ //')"
echo "Memória Total: $(free -h | awk '/Mem/ {print $2}')"
echo "Disco(s):"
df -h | grep -E '^/dev/' | awk '{print $1 "\t" $2 "\t" $3 "\t" $4}'

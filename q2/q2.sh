#!/bin/bash

# Pergunta ao usuário o nome de 3 diretórios
read -p "Digite o nome do primeiro diretório: " d1
read -p "Digite o nome do segundo diretório: " d2
read -p "Digite o nome do terceiro diretório: " d3

# Contadores para os diferentes tipos de arquivos
contador_xls1=0
contador_bmp1=0
contador_docx1=0
contador_xls2=0
contador_bmp2=0
contador_docx2=0
contador_xls3=0
contador_bmp3=0
contador_docx3=0

# Conta os arquivos .xls, .bmp e .docx no primeiro diretório
contador_xls1=$(find "$d1" -type f -name "*.xls" | wc -l)
contador_bmp1=$(find "$d1" -type f -name "*.bmp" | wc -l)
contador_docx1=$(find "$d1" -type f -name "*.docx" | wc -l)

# Conta os arquivos .xls, .bmp e .docx no segundo diretório
contador_xls2=$(find "$d2" -type f -name "*.xls" | wc -l)
contador_bmp2=$(find "$d2" -type f -name "*.bmp" | wc -l)
contador_docx2=$(find "$d2" -type f -name "*.docx" | wc -l)

# Conta os arquivos .xls, .bmp e .docx no terceiro diretório
contador_xls3=$(find "$d3" -type f -name "*.xls" | wc -l)
contador_bmp3=$(find "$d3" -type f -name "*.bmp" | wc -l)
contador_docx3=$(find "$d3" -type f -name "*.docx" | wc -l)

# Exibe os resultados
echo "Quantidade de arquivos .xls no primeiro diretório: $contador_xls1"
echo "Quantidade de arquivos .bmp no primeiro diretório: $contador_bmp1"
echo "Quantidade de arquivos .docx no primeiro diretório: $contador_docx1"

echo "Quantidade de arquivos .xls no segundo diretório: $contador_xls2"
echo "Quantidade de arquivos .bmp no segundo diretório: $contador_bmp2"
echo "Quantidade de arquivos .docx no segundo diretório: $contador_docx2"

echo "Quantidade de arquivos .xls no terceiro diretório: $contador_xls3"
echo "Quantidade de arquivos .bmp no terceiro diretório: $contador_bmp3"
echo "Quantidade de arquivos .docx no terceiro diretório: $contador_docx3"

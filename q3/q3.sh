#!/bin/bash

# ------------------------------------------------------
# Explicação: Criando Variáveis no Bash
# ------------------------------------------------------

# Forma 1: Atribuindo um valor diretamente
v1="Exemplo de variável criada diretamente."

# Forma 2: Atribuindo o resultado de um comando
v2=$(whoami)

# Forma 3: Usando o comando 'read' para solicitar um valor ao usuário
read -p "Digite um valor para v3: " v3

# ------------------------------------------------------
# Explicação: Diferença entre Entrada do Usuário e Parâmetros
# ------------------------------------------------------

# Entrada do Usuário (interativo)
echo "Você inseriu: $v3"

# Parâmetros de Linha de Comando
echo "O primeiro argumento da linha de comando é: $1"
echo "O segundo argumento da linha de comando é: $2"

# ------------------------------------------------------
# Explicação: Variáveis Automáticas
# ------------------------------------------------------

# Exemplos de Variáveis Automáticas
echo "Nome do Script: $0"
echo "Número de Argumentos: $#"
echo "Lista de Argumentos: $@"
echo "Último Status de Saída: $?"

# ------------------------------------------------------
# Fim do Script
# ------------------------------------------------------



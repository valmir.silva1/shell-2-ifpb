#!/bin/bash

# Pedir ao usuário para inserir os números
echo "Digite o primeiro número:"
read n1

echo "Digite o segundo número:"
read n2

echo "Digite o terceiro número:"
read n3

# Soma os três números
r=$(echo "$n1 + $n2 + $n3" | bc)

# Imprime o resultado
echo "A soma é: $r"


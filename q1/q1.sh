#!/bin/bash

echo "No alto daquela serra, oh que saudade me dá"
sleep 2
echo "Daquelas tardes bonitas, com o gado a descansar"
sleep 2
echo "Deitado à sombra fresca, na brisa que vinhá"
sleep 2
echo "Ouvindo o canto das aves, nas árvores a soluçar"
sleep 2
echo "Nos olhos trago a tristeza, que o tempo não pode apagar"
sleep 2
echo "Recordo a casa simples, onde um dia pude morar"
sleep 2
echo "A chaminé sempre acesa, o fogão a estalar"
sleep 2
echo "As mãos calejadas do pai, na lida a trabalhar"
sleep 2
echo "E a mãe sempre sorrindo, com amor a nos guiar"
sleep 2
echo "Na simplicidade da vida, aprendi a amar"
sleep 2
echo "Essas memórias queridas, jamais irão se apagar"






